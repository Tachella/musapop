## MuSAPoP algorithm (MATLAB codes)

**Paper title:** 
"Bayesian 3D Reconstruction of subsampled multispectral Lidar signals"

**Authors:**
J. Tachella, Y. Altmann, M. Marquez, H. Arguello-Fuentes, J-Y. Tourneret, S. McLaughlin

**Published in:**
IEEE Transactions on Computational Imaging, 2019

**Link to pdf:**
(to be updated)

## How to run this demo
1. Open code in MATLAB
2. Open script run_example.m
3. Choose number measured wavelengths per pixel (has to be a power of 2 and smaller than the total number of wavelengths)
4. Run the script. 

If another dataset is desired, just uncomment the desired dataset in run_example

## Trying the code with your data
Add to the folder 'data' a my_data.mat file containing:
1. A array Y (double) containing the Lidar histograms of size(Y) = [Nr, Nc, T] where Nr and Nc are the number of vertical and horizontal pixels respectively, and T is the number of histogram bins
2. A vector h (double) containing the system's IRF of size(h) = [T, 1], scaled such that sum(h) equals the number of signal photons recorded during the calibration of the IRF with a >95% reflective calibration panel.
3. Run the script run\_detection.m, selecting the file my_data.mat

## Requirements
MATLAB (tested only in v. 2018a)


