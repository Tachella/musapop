clear all; close all; clc;
%%


scenarios = [3];
band_per_pix = [4];
sizes = [198,66,22];
L = 32;
for j=1:length(scenarios)
    for k=1:length(band_per_pix)
        G = sampling_matrix(198, 198, L, band_per_pix(k), scenarios(j));
        save(['codes_sc' num2str(scenarios(j)) '_bands' num2str(band_per_pix(k))],'G')
    end
end
