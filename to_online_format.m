clear all; close all; clc;
%%
filename = 'data/middlebury_art';
load(filename)

L = length(Y);
Ncol = length(Y{1})/Nrow;
Y_out = cell(L,1);
for l=1:L
   Y_out{l} = cell(Nrow,Ncol);
   for n = 1:Nrow*Ncol
       Y_out{l}{n} = [Y{l}{n}',Y_ind{l}{n}']; % get time-bin
   end
end
   
wavelengths = []; bin_width = 0.3;

Y = Y_out;
save(filename,'Y','scale_ratio','wavelengths','bin_width','T','h')