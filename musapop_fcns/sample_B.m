function [B_new,map_delta,mdB] = sample_B(Y,Y_ind,T,h,attack,decay,A,T0,B,B_prior,G)

[N,L] = size(G);

B_new = zeros(N,L);
map_delta = 0;

%% sample pixels
for l=1:L
    for n=1:N 
        if G(n,l)>0
            ind = Y_ind{l}{n};
            if ~isempty(ind)
                a = A{n};
                t0 = T0{n};
                x_curr = zeros(length(ind),1);
                for j=1:length(t0)
                    i1 = find(ind>t0(j)-attack & ind<t0(j)+decay);
                    if ~isempty(i1)
                        x_curr(i1)=x_curr(i1)+exp(a(j,l))*h(ind(i1)-t0(j)+attack,l);
                    end
                end
                y=Y{l}{n};
                ybn = sum(y(x_curr==0));
                if sum(x_curr>0) > 0
                    ybn = ybn + sample_photon_sum(y(x_curr>0)',x_curr(x_curr>0),B(n,l));
                end
                B_new(n,l) = gamrnd(B_prior(n,l,1)+ybn,B_prior(n,l,2)/(G(n,l)*T*B_prior(n,l,2)+1));
                map_delta = map_delta +  y*(log(B_new(n,l)+x_curr)-log(B(n,l)+x_curr));
            else
                B_new(n,l) = gamrnd(B_prior(n,l,1),B_prior(n,l,2)/(G(n,l)*T*B_prior(n,l,2)+1)); 
            end
        else
            B_new(n,l) = gamrnd(B_prior(n,l,1),B_prior(n,l,2)); 
        end
        
        
    end
end


%% MAP delta computation 
map_delta = map_delta - T*sum(sum(G.*(B_new-B)));
mdB = sum(sum((log(B_new)-log(B)).*(B_prior(:,:,1)-1)))- sum(sum((B_new-B)./B_prior(:,:,2)));
map_delta= map_delta + mdB;

    
end