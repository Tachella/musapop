function  [n_col,n_row]=select_seq_neighbor_2(Nrow,Ncol,n_row,n_col,iter,factor)


    c = ceil(iter/factor);
    r = iter - (c-1)*factor;
    r = r - ceil(factor/2);
    c = c - ceil(factor/2);
    if r==0 && c==0
        r=floor(factor/2);
        c=floor(factor/2);
    end

    
    n_col=n_col+c;
    n_row=n_row+r;
    
    if n_col>Ncol || n_row>Nrow || n_col<1 || n_row<1
        n_col=0; 
    end
end