function [A,T0,B,map_delta,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points] = dilate_move(Y,Y_ind,T,h,attack,decay,A,T0,B,prior_pixels,lambda_S,gamma_strauss,max_dist,occupied_volume,prior_length,PPP,total_points,integrated_h,Npix,Nbin,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,prob_dilate,prob_erode,sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels,B_prior,SBR)
    
    mdB=0;
    map_delta=0;
    L = size(G,2);
    N=length(Y{1});
    Ncol=N/Nrow;
    %% pick a pixel from existing ones
    accepted=false;
    while ~accepted
        [center_pixel,index] = choose_random_point(PPP,sum_PPP);
        neigh=NEIGH{center_pixel};
        bord = is_border(center_pixel,Nrow,Ncol);
        if neigh(index) < 8 - (bord>0) -2*bord
           accepted=true;
        end
    end
    
   % nnnn=neigh(index);
    
    %% pick a neighboring pixel
    n_col = ceil(center_pixel/Nrow);
    n_row = center_pixel-(n_col-1)*Nrow;
    %%pick a bin
    t0_center_pix = T0{center_pixel};
    t0_center_pix=t0_center_pix(index);
    
    t0_new=randi([t0_center_pix-Nbin,t0_center_pix+Nbin]);
    
    order=randperm(8);
    strauss=Inf; p=1;
    while isinf(strauss) %% 
        pixel=select_seq_neighbor(Nrow,Ncol,n_row,n_col,order(p));
        if pixel
            t0 = T0{pixel};
            strauss=0;
            for j=1:length(t0)
                if abs(t0_center_pix-t0(j))<=Nbin
                    strauss=Inf;
                    break
                end
            end
        end
        p=p+1;
    end
    
    
    %% strauss term
    strauss=0;
    for j=1:length(t0)
        if abs(t0_new-t0(j))<=max_dist
            strauss=strauss+log(gamma_strauss);
        end
    end
    
    if ~isinf(strauss) && sum((t0_new-prior_pixels{pixel})==0)
       
        %% compute resulting neighbors
%         [neigh_sum_prime,new_neigh] = neighborhood_change(occupied_volume,pixel,t0_new,Nbin,NEIGH,T0,neigh_sum);
        
        a=A{pixel};
        b=B(pixel,:);
      
        %% propose a peak
        
        [prec, mean_a, det_term] = get_GP_par(t0_new,pixel,A,T0,alpha,scale_Z,Nrow,Nbin,L,false);

        u = rand(1,L);
        a_new = log(SBR.*b.*(1-u)*T./integrated_h);

        b_new = u.*b;

%         a_new = mean_a + randn(1,L)./sqrt(prec);
        
        
%         b_new = b-exp(a_new).*integrated_h/T;
        
%         if sum(b_new<0)==0 && sum(a_new<0)==0

            a_prop=a;
            t0_prop=t0;

            a_prop(end+1,:)=a_new;
            t0_prop(end+1,1)=t0_new;

            %% proposal logprobability
            prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);

            prop = prop + sum(-1/2.*prec*(mean_a-a_new).^2) + det_term;
            
            prop = prop - sum(G(pixel,:).*exp(a_new).*integrated_h.*(1-1./SBR));
            %% current logprobability
            curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);


            %% other GMRF term
            map_GMRF = pixelwise_B_prior(b_new,b,B_prior(pixel,:,1),B_prior(pixel,:,2)); 

            %% non sym dilation term
            n_col = ceil(pixel/Nrow);
            n_row = pixel-(n_col-1)*Nrow;
            total_neigh = occupied_volume(n_row,n_col,t0_new);
            term = -log(total_points/total_neigh)-log(2*Nbin+1);

            %% Poisson ref measure 
            ref_measure = log(lambda_S)-log(prior_length);
            
            %% non symmetrical proposal term
            nonsym=  - term   - log(points_with_neigh+1) - sum(log(1-u)) ;

            %% area interaction
            log_penalty = area_interaction(0,occupied_volume,pixel,t0_new,[],[],Npix,Nbin,log_gamma_area_int,lambda_area_int);

            %% accept/reject
            if rand<exp(prop-curr+strauss+log_penalty+map_GMRF+ref_measure+nonsym)
                %% save new estimates
                A{pixel}=a_prop;
                T0{pixel}=t0_prop;
                B(pixel,:) = b_new;
                
                total_points = total_points+1;
                Mergeable_pixels = update_mergeable_list(t0_prop,t0,max(attack)+max(decay),pixel,Mergeable_pixels);
                
                if ~isempty(a) %remove pixel from list of size(a,1) points
                   p=PPP{size(a,1)};
                   p(p==pixel)=[];
                   PPP{size(a,1)}=p;
                   sum_PPP(size(a,1))=sum_PPP(size(a,1))-1;
                end

                PPP{size(a_prop,1)}=[PPP{size(a_prop,1)};pixel];
                sum_PPP(size(a_prop,1))=sum_PPP(size(a_prop,1))+1;


                %% compute map
                map_delta=ref_measure+prop-curr+strauss+log_penalty+map_GMRF;

                mdB=map_GMRF;
                [occupied_volume,NEIGH,points_with_neigh,neigh_sum]=modify_volume(0,occupied_volume,pixel,t0_new,[],Npix,Nbin,NEIGH,T0,[],points_with_neigh,neigh_sum);
                eff_prior_length=modify_prior(0,prior_pixels,pixel,t0_new,max_dist,eff_prior_length);
            
                K(pixel,size(a_prop,1)+1)=K(pixel,size(a_prop,1)+1)+1;
            else
                K(pixel,size(a,1)+1)=K(pixel,size(a,1)+1)+1;
            end
%         end
    end
    
end