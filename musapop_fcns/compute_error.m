function compute_error(filename,output,bin_width,max_distance)

distance = 0:round(max_distance/bin_width);
load(filename);
A_true = output{1};
T0_true = output{2};
B_true = output{3};
tot_points = sum(cellfun(@length,T0_true));
A = output{1};
T0 = output{2};
B = output{3};

distance_mm = bin_width*distance;

T0_false = zeros(length(distance),1);
T0_correct = zeros(length(distance),1);
total_rae = zeros(length(distance),1);
L = size(B,2);
for i=1:length(distance)
    
    total_dae = 0;
    for n=1:length(A)
        [t0_false,t0_corr,rae,dae]  = find_matches(distance(i),T0{n},A{n},T0_true{n},A_true{n},L);
        T0_false(i) = T0_false(i) + t0_false;
        T0_correct(i) = T0_correct(i) + t0_corr;
        total_dae = total_dae + dae;
        total_rae(i) = total_rae(i) + rae;
    end
    
end

disp(['DAE at ' num2str(distance_mm(end)) 'mm : ' num2str(total_dae/T0_correct(end))])
T0_correct = T0_correct/tot_points*100;
total_rae = total_rae/tot_points;
avg_bkg_mse = mean(sum((B-B_true).^2,1)./sum(B_true.^2,1));
disp(['NMSE BKG: ' num2str(avg_bkg_mse*100) '%'])
subplot(131)
plot(distance_mm,T0_correct)
hold on
title('True points found [%]')
xlabel('distance [mm]')
xlim([distance_mm(1), distance_mm(end)])
subplot(132)
plot(distance_mm,T0_false)
title('False points')
xlabel('distance [mm]')
hold on
xlim([distance_mm(1), distance_mm(end)])
subplot(133)
plot(distance_mm,total_rae)
title('IAE [photons]')
xlabel('distance [mm]')
hold on
xlim([distance_mm(1), distance_mm(end)])

end