function [Y_out,Y_out_ind,T,G_new,Nrow,scale_ratio] = downsample_poisson(Y,Y_ind,T,G,factor,Nrow,scale_ratio)

L = length(Y);
N = length(Y{1});

if isempty(G)
   G=ones(N,L); 
end

if factor>1
    
    Ncol = N/Nrow;
    
    
    if size(G,3)==1
        G = reshape(G,Nrow,Ncol,L);
    end
    
    Y_out = cell(L,1);
    Y_out_ind = cell(L,1);
    
    Ncol_s = ceil(Ncol/factor);
    Nrow_s = ceil(Nrow/factor);
    
    for l=1:L
        Y_out{l} = cell(Nrow_s,Ncol_s);
        Y_out_ind{l} = cell(Nrow_s,Ncol_s);
        Y{l} = reshape(Y{l},Nrow,Ncol);
        Y_ind{l} = reshape(Y_ind{l},Nrow,Ncol);
    end
    
    G_new = zeros(Nrow_s,Ncol_s,L);
    
    for l=1:L
        k=1; p=1;
        for i=1:factor:Nrow
            for j=1:factor:Ncol
                
                y = zeros(1,T);
                for t1=i:i+factor-1
                    for t2=j:j+factor-1
                        if (t1<=Nrow) && (t2<=Ncol)
                            y(Y_ind{l}{t1,t2}) = y(Y_ind{l}{t1,t2})+Y{l}{t1,t2};
                        end
                    end
                end
                
                Y_out_ind{l}{k,p} = find(y);
                Y_out{l}{k,p} = y(y>0);
                G_new(k,p,l) = sum(sum(G(i:min([Nrow,i+factor-1]),j:min([Ncol,j+factor-1]),l)))/factor^2;
                p = p+1;
            end
            p=1;
            k=k+1;
        end
    end
    
    for l=1:L
        Y_out{l} = Y_out{l}(:);
        Y_out_ind{l} = Y_out_ind{l}(:);
    end
    G_new = reshape(G_new,Nrow_s*Ncol_s,L);
    
    Nrow = Nrow_s;
   
    scale_ratio = scale_ratio*factor;
else
    keyboard
    
end

end