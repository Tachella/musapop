function T0_prior=remove_borders(T0_prior)

Nrow=sqrt(length(T0_prior));

n_col=Nrow;
for n_row=1:Nrow
    pixel=n_row+(n_col-1)*Nrow;
    T0_prior{pixel}=[];
end

n_col=1;
for n_row=1:Nrow
    pixel=n_row+(n_col-1)*Nrow;
    T0_prior{pixel}=[];
end


n_row=Nrow;
for n_col=1:Nrow
    pixel=n_row+(n_col-1)*Nrow;
    T0_prior{pixel}=[];
end

n_row=1;
for n_col=1:Nrow
    pixel=n_row+(n_col-1)*Nrow;
    T0_prior{pixel}=[];
end

end