function Y_out = get_full_3D_mat(Y,Y_ind,T)

N = length(Y);

Y_out = zeros(N,T);

for n=1:N
   Y_out(n,Y_ind{n})=Y{n};    
end

end