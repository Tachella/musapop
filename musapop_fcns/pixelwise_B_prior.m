function  map_GMRF = pixelwise_B_prior(b_new,b,r_gmrf,theta_gmrf)
    map_GMRF = (log(b_new)-log(b))*(r_gmrf-1)' - (b_new-b)*(1./theta_gmrf)';
end