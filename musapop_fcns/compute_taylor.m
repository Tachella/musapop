function [u,c] = compute_taylor(B,mu,Y,T)

    
    c = T.*exp(B+mu);
    
    u = Y-T.*exp(B+mu) + c.*B;

end