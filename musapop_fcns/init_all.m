function [PPP,eff_prior_length]=init_all(T0,max_points_per_pixel,T0_prior,eff_prior_length,min_dist,N)


PPP=cell(max_points_per_pixel,1);
%% init PPP
for n=1:N
    t0=T0{n};
    for j=1:length(t0)
        eff_prior_length = modify_prior(0,T0_prior,n,t0(j),min_dist,eff_prior_length);
    end
    if ~isempty(t0)
        ppp=PPP{length(t0)};
        ppp(end+1)=n;
        PPP{length(t0)}=ppp(:);
    end
end

end