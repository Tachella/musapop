function [K,THETA] = GammaKL(logx,x)

s = log(x)-logx;

K = (3-s+sqrt((s-3).^2+24*s))./(12*s);
for i=1:10
   K = K - (log(K)-psi(K)-s)./(1./K-psi(1,K));
end


THETA = x./K;

end