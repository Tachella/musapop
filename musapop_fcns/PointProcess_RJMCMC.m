function output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,T0,A,B,log_gamma_area_int,lambda_area_int,G,alpha,Nrow,scale_Z,Nbin,plot_debug,B_prior,SBR,wavelengths)

%% display data stats and normalize impulse response
h = show_data_stats(Y,G,SBR,h);

integrated_h = sum(h,1);
for n=1:length(A)
   if ~isempty(A{n})
        A{n} = A{n} - ones(size(A{n},1),1)*log(integrated_h);
   end
end


%% Constants
N=length(Y{1});
L = length(Y);
Ncol=N/Nrow;
Nbi=ceil(Nmc/2);
Npix = 1;
max_points_per_pixel = 100;
window = ones(1e3,1);
bin_width = 1;

%% Hyperpriors
lambda_S = 1;
gamma_strauss = 0;
min_dist = 2*Nbin + 3;

%% proposals 
%prob_dilate=[1,2,3,4,5,6,7,8]*3;
prob_dilate = ones(1,8);
prob_dilate = prob_dilate/sum(prob_dilate);
%prob_erode = [8,7,6,5,4,3,2,1]*3;
prob_erode = ones(1,8);
prob_erode = prob_erode/sum(prob_erode);
beta_par = 1; % birth proposal
beta_ms = 1;
Mergeable_pixels = cell(max_points_per_pixel,1);

%% Storage
max_map=-Inf; log_map=zeros(Nmc,1);
if plot_debug
    birth_rate=zeros(Nmc,1); births=1;
    death_rate=zeros(Nmc,1); deaths=1;
    shift_rate=zeros(Nmc,1); shifts=1;
    erosion_rate = zeros(Nmc,1); erosions=1;
    dilation_rate = zeros(Nmc,1); dilations=1;
    merge_rate = zeros(Nmc,1); merges=1;
    split_rate = zeros(Nmc,1); splits=1;
    mark_rate = zeros(Nmc,1);  mark_moves=1;
    map_delta_debug=zeros(Nmc,7);
    map_B_debug=zeros(Nmc,1);
    map_A_debug=zeros(Nmc,1);
end
total_points=zeros(Nmc,1);
K = zeros(N,max_points_per_pixel);


%% Matched filter prior
prior_PPP = cellfun(@length,T0_prior); % candidate points per pixel
prior_length = sum(prior_PPP);
eff_prior_length = prior_length;

%% Initialize Gains
if isempty(G)
    G=ones(N,1);
elseif size(G,3)>1
    G= reshape(G,N,L);
end

%% Initialize variables
[PPP,eff_prior_length]=init_all(T0,max_points_per_pixel,T0_prior,eff_prior_length,min_dist,N);
total_points(1) = sum(cellfun(@length,T0));
occupied_volume = fill_occupied_volume(T0,T,Npix,Nbin,Nrow);
[NEIGH,neigh_sum] = find_all_neighbors(T0,occupied_volume,Nrow);
points_with_neigh = sum(neigh_sum);

sum_PPP = cellfun(@length,PPP);

if plot_debug
    figure(2)
    imagesc(reshape(prior_PPP,Nrow,Ncol)/T)
    colorbar
end
prior_order=0;


%% extend and conquer
prior_PPP = extend_prior_PPP(prior_PPP);

%% move probabilties
% mark shift birth death dilate erode split merge
p_moves = [10,10,1,1,1,1,1,1];
p_moves = p_moves/sum(p_moves);
p_moves = cumsum(p_moves);
new_max = 0;

%% first time sample U,B
[B,~,~] = sample_B(Y,Y_ind,T,h,attack,decay,A,T0,B,B_prior,G);
 
B_mean = zeros(size(B));
b_samples = 0;

%% Main Algo
if plot_debug
    tic
end

for iter=2:Nmc
    
    map_delta=0;
    total_points(iter) = total_points(iter-1);
     
    if plot_debug
        map_B_debug(iter) = map_B_debug(iter-1);
    end
    
    if ~rem(iter,N)
        %% sample background
        [B,md,mdB] = sample_B(Y,Y_ind,T,h,attack,decay,A,T0,B,B_prior,G);
        
        map_delta = map_delta+md;
        if plot_debug
            map_B_debug(iter) = map_B_debug(iter)+mdB;
        end
        
        if iter>Nbi
           B_mean = B_mean + B; 
           b_samples = b_samples+1;
        end
    end
    
    %% RJMCMC
    u=rand;
    move = 1;
    while u > p_moves(move)
       move = move+1;
    end
    
    if move == 1
        %% Mark move
        if total_points(iter-1)>0
            [A,md] = mark_move(Y,Y_ind,h,attack,decay,A,T0,B,integrated_h,PPP,G,Nbin,alpha,scale_Z,Nrow,sum_PPP,SBR);
            map_delta=map_delta+md;
            if plot_debug
                if md~=0
                   mark_rate(mark_moves)=1;
                else
                   mark_rate(mark_moves)=0;
                end
                mark_moves = mark_moves+1;
                map_A_debug(mark_moves)=map_A_debug(mark_moves-1)+md;
            end
        end
    elseif move == 2
        %% Shift
        if total_points(iter-1)>0
           [T0,md,occupied_volume,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,Mergeable_pixels]...
               = shift_move(Y,Y_ind,h,attack,decay,A,T0,B,T0_prior,gamma_strauss,min_dist, ...
               occupied_volume,PPP,Npix,Nbin,NEIGH,points_with_neigh,neigh_sum,eff_prior_length, ...
               log_gamma_area_int,lambda_area_int,G,sum_PPP,alpha,scale_Z,Nrow,T,Mergeable_pixels);
           
           map_delta=map_delta+md;
           if plot_debug
               shifts=shifts+1;
               if md~=0
                   shift_rate(shifts)=1;
               else
                   shift_rate(shifts)=0;
               end
             map_delta_debug(shifts,1)=map_delta_debug(shifts-1,1)+md;
           end
        end
        
    elseif move == 3 
        %% Birth
       [A,T0,B,md,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points(iter)] ...
           = birth_move(Y,Y_ind,T,h,attack,decay,A,T0,B,T0_prior,lambda_S,gamma_strauss,min_dist,occupied_volume, ...
           prior_length,prior_PPP,prior_order,PPP,total_points(iter),integrated_h,Npix,Nbin,beta_par,NEIGH,points_with_neigh,neigh_sum, ...
           eff_prior_length,sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels,B_prior,SBR);

       map_delta=map_delta+md;
       if plot_debug
           births=births+1;
           if md~=0
               birth_rate(births)=1;
           else
               birth_rate(births)=0;
           end
           map_delta_debug(births,2)=map_delta_debug(births-1,2)+md;
           map_B_debug(iter)=map_B_debug(iter)+mdB;
       end
    elseif move == 4
        %% Death
        if total_points(iter-1) > 0
           [A,T0,B,md,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points(iter)] ...
               = death_move(Y,Y_ind,T,h,attack,decay,A,T0,B,lambda_S,gamma_strauss,min_dist,occupied_volume, ...
               prior_length,PPP,integrated_h,Npix,Nbin,beta_par,NEIGH,points_with_neigh,neigh_sum,T0_prior,eff_prior_length, ...
               sum_PPP,log_gamma_area_int,lambda_area_int,G,total_points(iter),alpha,scale_Z,K,Nrow,Mergeable_pixels,B_prior,SBR);
          
           map_delta = map_delta + md;
           if plot_debug
                deaths=deaths+1;
               if md~=0
                   death_rate(deaths)=1;
               else
                   death_rate(deaths)=0;
               end
               map_delta_debug(deaths,3)=map_delta_debug(deaths-1,3)+md;
               map_B_debug(iter)=map_B_debug(iter)+mdB;
           end
        end
    elseif move == 5
        %% Dilate
        if total_points(iter-1)>0
           [A,T0,B,md,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points(iter)] ...
               = dilate_move(Y,Y_ind,T,h,attack,decay,A,T0,B,T0_prior,lambda_S,gamma_strauss,min_dist,  ... 
               occupied_volume,prior_length,PPP,total_points(iter),integrated_h,Npix,Nbin,NEIGH,points_with_neigh,neigh_sum, ...
               eff_prior_length,prob_dilate,prob_erode,sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels,B_prior,SBR);
           
           map_delta = map_delta+md;
           if plot_debug
               dilations = dilations+1;
               if md~=0 
                   dilation_rate(dilations)=1;
               else
                   dilation_rate(dilations)=0;
               end
               map_delta_debug(dilations,4)=map_delta_debug(dilations-1,4)+md;
               map_B_debug(iter)=map_B_debug(iter)+mdB;
           end
        end
    elseif move == 6
        %% Erode
        if points_with_neigh>0
           [A,T0,B,md,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points(iter)] ...
               = erode_move(Y,Y_ind,T,h,attack,decay,A,T0,B,NEIGH,lambda_S,gamma_strauss,min_dist,occupied_volume, ...
               prior_length,PPP,total_points(iter),integrated_h,Npix,Nbin,points_with_neigh,neigh_sum,T0_prior,eff_prior_length, ...
               prob_dilate,prob_erode,sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels,B_prior,SBR); 
           
           map_delta = map_delta+md;
           if plot_debug
               erosions = erosions+1;
               if md~=0
                   erosion_rate(erosions)=1;
               else
                   erosion_rate(erosions)=0;
               end
               map_delta_debug(erosions,5)=map_delta_debug(erosions-1,5)+md;
               map_B_debug(iter)=map_B_debug(iter)+mdB;
           end
        end
    elseif move==7
        %% Split
        if total_points(iter-1)>0
           [A,T0,md,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points(iter)]...
               = split_move(Y,Y_ind,T,h,attack,decay,A,T0,B,T0_prior,lambda_S,gamma_strauss,min_dist,occupied_volume, ...
               prior_length,prior_PPP,prior_order,PPP,total_points(iter),Npix,Nbin,NEIGH,points_with_neigh,neigh_sum,eff_prior_length, ...
               sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels,beta_ms);
           
           map_delta = map_delta+md;
           if plot_debug
               splits=splits+1;
               if md~=0
                   split_rate(splits)=1;
               else
                   split_rate(splits)=0;
               end
               map_delta_debug(splits,6)=map_delta_debug(splits-1,6)+md;
           end
           
        end
    else
        %% Merge
        if sum(cellfun(@length,Mergeable_pixels))>0
           [A,T0,B,md,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels] ...
              = merge_move(Y,Y_ind,T,h,attack,decay,A,T0,B,lambda_S,min_dist,occupied_volume,prior_length,PPP,Npix,Nbin, ...
               gamma_strauss,NEIGH,points_with_neigh,neigh_sum,T0_prior,eff_prior_length,sum_PPP,log_gamma_area_int, ...
               lambda_area_int,G,total_points(iter),alpha,scale_Z,K,Nrow,Mergeable_pixels,beta_ms);
           
           map_delta=map_delta+md;
           if plot_debug
               merges=merges+1;
               if md~=0
                   merge_rate(merges)=1;
               else
                   merge_rate(merges)=0;
               end
               map_delta_debug(merges,7)=map_delta_debug(merges-1,7)+md;
           end
        end
    end
   
    %%
    if iter==Nbi
        K = zeros(size(K)); 
        for n=1:N
           K(n,size(A{n},1)+1)=K(n,size(A{n},1)+1)+1; 
        end
    end
    
    %% MAP estimation
    log_map(iter) = log_map(iter-1) + map_delta;
    if iter==Nbi
        max_map = log_map(iter);
        T0_map = T0;
        A_map = A;
        NEIGH_map = NEIGH;
    end
    
    if log_map(iter)>max_map && iter>Nbi
        max_map = log_map(iter);
        T0_map = T0;
        A_map = A;
        NEIGH_map = NEIGH;
        new_max = new_max+1;
    end
    
    
    %% plot stuff
    if rem(iter,1e5)==0
        disp('------')
        disp(['complete: ' num2str(round(100*(iter/Nmc))) '%'])
        disp(['new maps: ' num2str(new_max)])
        disp(['total points: ' num2str(total_points(iter))])
       % disp(['mean background: ' num2str(mean(B))])
        if  plot_debug
            toc
            %if total_points(iter)~total_points(iter-1)
            disp(['neighboring points: ' num2str(points_with_neigh)])
            new_max=0;
            fig = 5;
            %total points
            figure(fig);
            subplot(311)
            plot(total_points);
            title('total points')
            % shifts/deaths/births
            subplot(312)
            hold off
            window=window/sum(window(:));
            %plot(cumsum(shift_rate(1:shifts))./(1:shifts)')
            plot(filter(window,1,shift_rate(1:shifts)))
            hold on
            plot(filter(window,1,mark_rate(1:mark_moves)))
            %plot(cumsum(birth_rate(1:births))./(1:births)')
            dd = filter(window,1,birth_rate(1:births));
            plot(dd)
            disp(['birth rate ' num2str(mean(dd)*100) '%'])
            %plot(cumsum(death_rate(1:deaths))./(1:deaths)')
            dd = filter(window,1,death_rate(1:deaths));
            plot(dd)
            disp(['death rate ' num2str(mean(dd)*100) '%'])
            dd = filter(window,1,dilation_rate(1:dilations));
            plot(dd)
            disp(['dilation rate ' num2str(mean(dd)*100) '%'])
            dd = filter(window,1,erosion_rate(1:erosions));
            plot(dd)
            disp(['erosion rate ' num2str(mean(dd)*100) '%'])
            dd = filter(window,1,split_rate(1:splits));
            plot(dd)
            disp(['split rate ' num2str(mean(dd)*100) '%'])
            dd = filter(window,1,merge_rate(1:merges));
            plot(dd)
            disp(['merge rate ' num2str(mean(dd)*100) '%'])
            legend('shifts','marks','births','deaths','dilations','erosions','splits','merges')
            % plot map
            subplot(313)
            plot(log_map(1:iter))
            title('map')
            % plot B
            fig=fig+1;
            figure(fig);
            for l=1:L
                subplot(ceil(sqrt(L)),ceil(sqrt(L)),l)
                imagesc(reshape(B(:,l),Nrow,Ncol));
                axis image
                axis off
            end
            % Plot points
            fig = fig+1;
            fig = plot_triang(T0,A,scale_Z,scale_Z*3,fig,Nrow,bin_width,wavelengths);
            % map B
            fig=fig+1;
            figure(fig);
            plot(map_B_debug)
            title('map B')
            % map b+d+s+m
            fig=fig+1;
            figure(fig);
            hold off
            plot(map_A_debug(1:mark_moves))
            hold on
            plot(map_delta_debug(1:shifts,1))
            %plot(map_delta_debug(1:births,2))
            plot(sum(map_delta_debug(1:min([births,deaths]),2:3),2))
            plot(sum(map_delta_debug(1:min([erosions,dilations]),4:5),2))
            plot(sum(map_delta_debug(1:min([splits,merges]),6:7),2))
            legend('map A','map shifts','map births+deaths','dilation+erosion','splits+merges')
            fig=fig+1;
            piks=cellfun(@length,T0);
            piks=reshape(piks,Nrow,Ncol);
            figure(fig)
            subplot(131)
            imagesc(piks==0)
            axis off
            axis image
            title('pixels without peaks')
            subplot(132)
            imagesc(piks==1)
            axis off
            axis image
            title('pixels with 1 peaks')
            subplot(133)
            imagesc(piks==2)
            axis off
            axis image
            title('pixels with 2 peaks')
            fig=fig+1;
            figure(fig)
            subplot(131)
            imagesc(reshape(K(:,1)./sum(K,2),Nrow,Ncol))
            axis off
            axis image
            title('pixels without peaks')
            subplot(132)
            imagesc(reshape(K(:,2)./sum(K,2),Nrow,Ncol))
            axis off
            axis image
            title('pixels with 1 peaks')
            subplot(133)
            imagesc(reshape(K(:,3)./sum(K,2),Nrow,Ncol))
            axis off
            axis image
            title('pixels with 2 peaks')
            fig=fig+1;
            figure(fig)
            imagesc(reshape(sum(K,2),Nrow,Ncol))
            axis off
            axis image
            colorbar
            fig=fig+1;
            figure(fig)
            stem(neigh_sum)
%             figure(fig+1)
%             histogram(cell2mat(A))
            % pause for a bit
            pause(0.1)
            tic
        end
    end
    
end

%% remove small points
[A_map, T0_map, NEIGH_map] = remove_lonely_points(A_map,T0_map,NEIGH_map);

for n=1:length(A_map)
   if ~isempty(A_map{n})
        A_map{n} = A_map{n} + ones(size(A_map{n},1),1)*log(integrated_h);
   end
end

%% save stuff
output{1} = A_map;
output{2} = T0_map;
output{3} = B_mean/b_samples;
output{4} = NEIGH_map;
output{5} = K;
output{6} = neigh_sum;
output{7} = scale_Z;
output{8} = h;

end