function [priorB,SBR] = build_prior_B(Y,Y_ind,T,G,T0,Nrow,h,attack,decay)

L = length(Y);
N = length(Y{1});

priorB = zeros(N,L,2);
SBR = zeros(1,L);
%% find regions without returns
% b_region = find_no_return_region(T0,Y,Y_ind,T,attack,decay);
% Tb = sum(b_region);

for l=1:L
    %% extract non-return region
    [YB,YS,Tb] = extract_background(T0,Y{l},Y_ind{l},T,attack,decay);
    mask = YB>prctile(YB,98);
    Tb(mask)=0;
    YB(mask)=0;
    %% sample correlated model
    [meanB,logB] = sample_corrB(YB,Tb,G(:,l),Nrow);
     
    %% minimize KL div with non-correlated model
    [priorB(:,l,1),priorB(:,l,2)] = GammaKL(logB,meanB);
     
    b = mean(meanB);
    SBR(l) = abs(sum(YS)/sum(G(:,l))-b*(T-mean(Tb)))/(b*T);
end

disp('SBR: ')
disp(SBR')
end