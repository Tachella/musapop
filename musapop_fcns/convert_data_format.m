function [Y_out,Y_ind,Nrow,Ncol,L] = convert_data_format(Y)

if iscell(Y)
   L = length(Y);
   [Nrow,Ncol] = size(Y{1});
   Y_out = cell(L,1);
   Y_ind = cell(L,1);
   for l=1:L
       Y_out{l} = cell(Nrow*Ncol,1);
       Y_ind{l} = cell(Nrow*Ncol,1);
       for n = 1:Nrow*Ncol
           Y_out{l}{n} = Y{l}{n}(:,1)'; % get photon count per time-bin
           Y_ind{l}{n} = Y{l}{n}(:,2)'; % get time-bin
       end
   end
else % Y should be a data hypercube of [Nrow pixels x Ncol pixels x T bins x L wavelengths]
   [Nrow,Ncol,T,L] = size(Y);
   [Y_out,Y_ind] = get_return_list(Y);
end


end