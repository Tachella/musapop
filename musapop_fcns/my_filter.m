function B = my_filter(B,h,Nrow,Ncol)
    B = ifft2(fft2(reshape(B,Nrow,Ncol)).*h);
    B = B(:);
end