function B = posterior_mode(B,mu,Y,T,h_q,Precond,Nrow,Ncol)

%% gradient descent


g = @(B) -Y+T.*exp(B+mu)+my_filter(B,h_q,Nrow,Ncol);
tol = 1e-2;  maxit = 100;

for i=1:1e3
    grad = g(B);
    
    gg = grad'*grad;
    if gg/length(B)<0.1
        break
    end
    
%% Hessian update
    [~,c] = compute_taylor(B,mu,Y,T);
    Q_fun = @(x) (my_filter(x,h_q,Nrow,Ncol)+c.*x); 
    [B_delta,~] = pcg(Q_fun,grad,tol,maxit,Precond);
    
    B = B - B_delta;
    
end


if sum(isnan(B))
    keyboard
end
end