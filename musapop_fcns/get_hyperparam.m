function [Nmc,lambda_area_int,log_gamma_area_int,alpha,Nbin]=get_hyperparam(length_Y,scale,scale_ratio)

log_gamma_area_int = 2;

alpha = (0.5)^2; % it is for the log-image!
Nmc = length_Y*(22-(scale-1)*7)*9^(scale-1); %% remove the first 3 for efficiency
lambda_area_int = log(length_Y)*1.2;
Nbin = round(8*scale_ratio/3^(scale-1));
%Nbin = round(12*scale_ratio/3^(scale-1));

end