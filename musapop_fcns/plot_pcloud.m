function plot_pcloud(p)

    if ~isempty(p.wavelengths)
        R = spectrumRGB(p.wavelengths);
    end
    
    points=sum(sum(cellfun(@(x) size(x,1),p.spectra)));
    T0_plot = zeros(points,1);
    A_plot = zeros(points,3);
    [Nrow,Ncol] = size(p.spectra);
    k=1;
    min_t0 = Inf;
    
    
    for i=1:Nrow*Ncol
        l=length(p.depth{i});
        if l>0
            T0_plot(k:k+l-1,1) = i;
            T0_plot(k:k+l-1,2) = p.depth{i};
            if min(p.depth{i})<min_t0
                min_t0 = min(p.depth{i});
            end
            
            if ~isempty(p.wavelengths)
                A_plot(k:k+l-1,:) = exp(p.spectra{i})*R;   
            else
                A_plot(k:k+l-1,:) = exp(p.spectra{i}(:,1:3));   
            end
            
            k=k+l;
        end
    end
    T0_plot(:,2) = T0_plot(:,2)-min_t0;
    
    
    x = (T0_plot(:,2))*p.bin_width;
    y = (Nrow-ceil(T0_plot(:,1)/Nrow))*p.scale_ratio*p.bin_width;
    z = (Nrow - (T0_plot(:,1) - Nrow*(ceil(T0_plot(:,1)/Nrow)-1)))*p.scale_ratio*p.bin_width;
    
%%   3D plot
    pcshow([x(:),y(:),z(:)],A_plot/mean(A_plot(:))/4);
    set(gcf,'color','w');
    axis image
    axis on
    xlabel('[mm]')
   

end