function [t0_false,t0_correct,rae,dae] = find_matches(distance,t0,a,t0_true,a_true,L)

t0_correct = 0;
t0_false = 0;
rae = zeros(1,L);
dae = 0;

j=1;
while(j<=length(t0))
    [dd,ind] = min(abs(t0(j)-t0_true));
   if dd<distance
       t0_correct = t0_correct+1;
       rae = rae + abs(exp(a(j,:))-exp(a_true(ind,:)));
       dae = dae + abs(t0(j)-t0_true(ind));
       t0_true(ind)=[]; a_true(ind,:)=[]; t0(j)=[]; a(j,:)=[];
       j = j - 1;
   else
       rae = rae + exp(a(j,:));
       t0_false = t0_false + 1;
   end
    j = j + 1;
end
if ~isempty(a_true)
    rae = rae + sum(exp(a_true),1);
end

rae = mean(rae);