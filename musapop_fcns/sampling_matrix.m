function [ G ] = sampling_matrix(Nrow, Ncol, L, band_per_pix, scenario)


if 0 == mod(L,band_per_pix)
    switch scenario
        % Full scans
        case 1
            G = ones(Nrow*Ncol,L);
        % Regular subsampling, without overlap    
        case 2
        if (sqrt(L)-round(sqrt(L)))==0
            C = zeros(sqrt(L),sqrt(L),L);
            cont = 1;
           for i=1:sqrt(L)
               for j=1:sqrt(L)
                   C(i,j,cont) = 1;
                   cont = cont + 1;
               end
           end
           for i=1:L
               G(:,:,i) = kron(ones(Nrow/sqrt(L),Ncol/sqrt(L)),C(:,:,i));
           end
           G = reshape(G(:),[Nrow*Ncol L]);
        end 
        % Regular subsampling, without overlap
        case 3
            G = zeros(Nrow*Ncol,L);
            for i=1:Nrow*Ncol
                G(i,randsample(L,band_per_pix))=1;
            end
        % Random subsampling, with overlap    
        case 4
            G = zeros(Nrow*Ncol,L);
            
            for i=1:L
               G(randsample(Nrow*Ncol,round(Nrow*Ncol/L*band_per_pix)),i) = 1;
            end
        %Blue noise codes
        case 5
            G = [];
            for i=1:band_per_pix
                gdmd1 = Run_claudiaCodesIter(max(Nrow,Ncol),L/band_per_pix);
                gdmd1 = gdmd1(1:Nrow,1:Ncol,:);
                gdmd1 = reshape(gdmd1(:),[Nrow*Ncol L/band_per_pix]);
                G = [G gdmd1];
            end

    end
    G = reshape(G(:),[Nrow*Ncol L]);
else
   disp('ERROR: The number of bands per pixel should be a factor of L') 
end
    
end

