function [Y_out,Y_ind,T] = get_return_list(Y)

if length(size(Y))==4
   Y = reshape(Y,size(Y,1)*size(Y,2),size(Y,3),size(Y,4)); 
end

[N,T,L]=size(Y);

Y_out = cell(L,1);
Y_ind = cell(L,1);

for l=1:L
    y_ind = cell(N,1);
    y_out = cell(N,1);
    for n=1:N
       y_ind{n} = find(Y(n,:,l));
       y_out{n} = Y(n,y_ind{n},l);
    end
    Y_ind{l} = y_ind;
    Y_out{l} = y_out;
end


end