function [A,map_delta]=mark_move(Y,Y_ind,h,attack,decay,A,T0,B,integrated_h,PPP,G,Nbin,alpha,scale_Z,Nrow,sum_PPP,SBR)
  
    sigma_RWM = sqrt(alpha);
    L = size(G,2);
    
    [pixel,index] = choose_random_point(PPP,sum_PPP);

    t0=T0{pixel};  a=A{pixel};
    b=B(pixel,:);
    g=G(pixel,:);
    
    
    [prec, mean_a]= get_GP_par(t0(index), pixel, A, T0, alpha, scale_Z, Nrow, Nbin, L, true);
    
    
    a_prop = a;
    
%     sumy = zeros(1,L);
%     for l=1:L
%         if g(l)
%             ind = Y_ind{l}{pixel};
%             for j=1:length(t0)
%                 i2 = (ind>t0(j)-attack & ind<t0(j)+decay);
%                 sumy(l) = sum(Y{l}{pixel}(i2));
%             end
%         end
%     end
%     sigma_RWM = 1./(prec+integrated_h);
%     mean_RWM = (mean_a+sumy.*(SBR./(1+SBR))).*sigma_RWM;
%     a_prop(index,:) = mean_RWM + randn(1,L).*sqrt(sigma_RWM);
    
%     proposal = 1/2*(((a(index,:)-mean_RWM).^2-(a_prop(index,:)-mean_RWM).^2)./sigma_RWM); 
  a_prop(index,:) = a(index,:)+randn(1,L)*sigma_RWM;
    
    %% current log probability
    curr = compute_likelihood_mark(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);
    prior_curr = -1/2*(a(index,:)-mean_a).^2.*prec;
    
    %% proposal log_prob
    prop = compute_likelihood_mark(Y,Y_ind,G,pixel,b,t0,a_prop,h,attack,decay);
    prior_prop = -1/2*(a_prop(index,:)-mean_a).^2.*prec;
    
    %% accept-reject
    likelihood = prop-curr-(exp(a_prop(index,:))-exp(a(index,:))).*g.*integrated_h;
    ind = exp(likelihood + prior_prop - prior_curr) > rand(1,L);
    
    a(index,ind) = a_prop(index,ind);
    
    %% save A
    A{pixel} = a;
    %% map delta
    map_delta = sum(prior_prop(ind)-prior_curr(ind)+likelihood(ind));
    
end