function [suma]=neigvorlocal_claudia(gdmd,N,n1,n2,j,h)
% n1=n1-1;
% n2=n2-1;
p1=max(1,j-floor(n1/2));
p2=min(N,j+floor(n1/2));
p3=max(1,h-floor(n2/2));
p4=min(N,h+floor(n2/2));
u=gdmd(p1:p2,p3:p4);
suma=sum((u(:)));


