function h=show_data_stats(Y,G,SBR,h)

    L = length(Y);
    photons = zeros(1,L);
    for l=1:L
        photons(l)=sum(cellfun(@sum,Y{l}));
    end
    
    mean_per_pixel = photons./size(G,1);
    disp('------------NEW SCALE-------------')
    for l=1:L
        disp(['mean photon per pix band ' num2str(l) ': ' num2str(mean_per_pixel(l))])
    end
    
    %% mean value
    h = h./ (ones(size(h,1),1)*sum(h));
    mean_value = 5;
    h = h .* (mean_per_pixel./(1+1./SBR))/mean_value;
    
end