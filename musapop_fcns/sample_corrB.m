function [meanB,logB] = sample_corrB(Y,Tb,G,Nrow)

Ncol=length(Y)/Nrow;

Tb = Tb.*G;

max_iter = 1000;

accepted = 0;
alpha = 1;
while (accepted < max_iter/100)
    accepted = 0;
    alpha = alpha*10;
    
    h_d = sqrt(alpha)*fspecial('laplacian',0);
    
    h = zeros(Nrow,Ncol);
    h(1:3,1:3) = h_d;
    h = circshift(h,[-1,-1]);
    h_d = real(fft2(h));
    h_q = h_d.*h_d;
    
    h_pre = 1./(h_q+1);
    
    B = log(abs(my_filter(Y,h_pre,Nrow,Ncol))./(Tb+1));
    
    logB = zeros(size(B));
    meanB = logB;
    Precond = @(x) my_filter(x,h_pre,Nrow,Ncol);
    
    B = posterior_mode(B,0,Y,Tb,h_q,Precond,Nrow,Ncol);
    mu = mean(B(:));
    B = B - mu;
    
    
    B = posterior_mode(B,mu,Y,Tb,h_q,Precond,Nrow,Ncol);
    [u,c] = compute_taylor(B,mu,Y,Tb);
    c(c<=0) = eps;
    Q_fun = @(x) (my_filter(x,h_q,Nrow,Ncol)+c.*x);
    tol = 1e-2;  maxit = 100;
    
    
    %tic
    for i=1:max_iter
        
        x = my_filter(randn(size(B)),h_d,Nrow,Ncol) + randn(size(B)).*sqrt(c) + u;
        [B_new,~] = pcg(Q_fun,x,tol,maxit,Precond);
        
        rho = delta_posterior(B+mu,B_new+mu,Y,Tb)+delta_gmrf(B_new,B,c,u);
        
        %% accept reject
        if exp(rho)>rand
            accepted = accepted+1;
            B = B_new;
        end
        
        logB = logB + B+mu;
        meanB = meanB + exp(B+mu);
        
        if ~rem(i,500)
            disp(['background prior MCMC acceptance ratio: ' num2str(accepted/i*100) '%'])
        end
        
    end
end

meanB = meanB/max_iter;
logB = logB/max_iter;

end