function [T0_prior,T0,A,B] = preprocess3D(Y,Y_ind,T,G,h,attack,decay,lowpass_size,Nrow,Nbin)

    
    attack = attack(1);
    decay = decay(1);
    L = length(Y);
    N = length(Y{1});
    t_prior = zeros(N,T);
    average = 0;
    
    for l=1:L
        Y_d = get_full_3D_mat(Y{l},Y_ind{l},T);
        sumY = sum(Y_d,2);
        average  = average + mean(sumY);
        %% match filtering
        t_prior_l = prior3D(Y_d,h(:,l),attack,decay,lowpass_size,Nrow);
        t_prior = t_prior + t_prior_l;
    end
    
    t_min = max([attack+1,Nbin+1]);
    t_max = T;
        
    %% thresholding prior
    m = average/5;
    g = mean(G,2);
    t_prior(t_prior<m*g)=0;
    t_prior(:,1:t_min)=0;
    t_prior(:,t_max:end)=0;
    
    
    T0 = cell(N,1);
    B = zeros(N,L);
    A = cell(N,1);
    for n=1:N
        [~,t0]= max(t_prior(n,:));
        a = 0.1*ones(1,L);
        for l = 1:L
            if G(n,l)>0
                ind = Y_ind{l}{n};
                ind = (ind> t0-attack & ind<t0+decay-1);
                a(l) = sum(Y{l}{n}(ind))/G(n,l);
            end
        end
        
        if mean(sum(a))>average*0.1
            B(n,:) = B(n,:)-a;
            a = log(a);
            A{n} = a;
            T0{n} = t0;
        end
    end
    
    B(B<=0) = 0.1;
    B = B/T;

    
    
    T0_prior = cell(N,1);
   % prior_length=0;
    for n=1:N
       T0_prior{n} = find(t_prior(n,:)>0)';
    end
    
    
    %% space to explore
    disp(['space to explore ' num2str(sum(sum(cellfun(@length,T0_prior)))/N/T*100) '%'])

end