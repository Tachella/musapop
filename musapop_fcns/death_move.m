
function [A,T0,B,map_delta,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points]=death_move(Y,Y_ind,T,h,attack,decay,A,T0,B,lambda_S,gamma_strauss,max_dist,occupied_volume,prior_length,PPP,integrated_h,Npix,Nbin,beta_par,NEIGH,points_with_neigh,neigh_sum, ...
    prior_pixels,eff_prior_length,sum_PPP,log_gamma_area_int,lambda_area_int,G,total_points,alpha,scale_Z,K,Nrow,Mergeable_pixels,B_prior,SBR)
%%
    mdB=0;
    map_delta = 0;
    L = size(B,2);
    
    [pixel,index] = choose_random_point(PPP,sum_PPP);
    t0=T0{pixel};  a=A{pixel};
    b=B(pixel,:);  

    a_prop=a;
    t0_prop=t0;

    a_prop(index,:)=[];
    t0_prop(index)=[];

    b_new=exp(a(index,:)).*integrated_h./SBR/T+b;
    u= b./b_new;
    
    %% proposal logprobability
    prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);
    
    prop = prop+sum(G(pixel,:).*exp(a(index,:)).*integrated_h.*(1-1./SBR));
    
    %% current logprobability
    curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);
    
    [prec, mean_a, det_term] = get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,L,false);
    curr = curr-sum((a(index,:)-mean_a).^2.*prec/2)+det_term;
    
    %% other GMRF term
    map_GMRF = pixelwise_B_prior(b_new,b,B_prior(pixel,:,1),B_prior(pixel,:,2)); 
    
    %% non symmetrical proposal term
    % jacobian (1-u)
    sym= -log(lambda_S)+log(total_points)+sum(log(1-u))+log(eff_prior_length-(2*max_dist+1))-log(prior_length); %+(beta_par-1)*log(1-u)+log(beta_par)

    %% area interaction
    log_penalty = area_interaction(1,occupied_volume,pixel,[],[],t0(index),Npix,Nbin,log_gamma_area_int,lambda_area_int);

    %% accept reject
    if rand<exp((prop-curr+log_penalty+map_GMRF)+sym)
       A{pixel}=a_prop;
       T0{pixel}=t0_prop(:);
       B(pixel,:)=b_new;
        
       Mergeable_pixels = update_mergeable_list(t0_prop,t0,max(attack)+max(decay),pixel,Mergeable_pixels);
        
       %remove pixel from list of size(a,1) points
       p=PPP{size(a,1)};
       p(p==pixel)=[];
       PPP{size(a,1)}=p;
       sum_PPP(size(a,1))=sum_PPP(size(a,1))-1;
        
        if ~isempty(a_prop) %if still has a point increment list
           PPP{size(a_prop,1)}=[PPP{size(a_prop,1)};pixel];
           sum_PPP(size(a_prop,1))=sum_PPP(size(a_prop,1))+1;
        end

        %% map delta
        map_delta=prop-curr+log_penalty-log(lambda_S/prior_length)+map_GMRF;

        mdB = map_GMRF;
        
        total_points = total_points-1;
        %% modify volume
        [occupied_volume,NEIGH,points_with_neigh,neigh_sum] = modify_volume(1,occupied_volume,pixel,[],t0(index),Npix,Nbin,NEIGH,T0,index,points_with_neigh,neigh_sum);
        eff_prior_length = modify_prior(1,prior_pixels,pixel,t0(index),max_dist,eff_prior_length);
        
        K(pixel,size(a_prop,1)+1)=K(pixel,size(a_prop,1)+1)+1;
    else
        K(pixel,size(a,1)+1)=K(pixel,size(a,1)+1)+1;
    end
    
    
end