function plot_bkg(B,wavelengths)

if ~isempty(wavelengths)
    R = spectrumRGB(wavelengths);
    b = B*R;
else
    b = B(:,:,1:3); 
end

imagesc(b/max(b(:)));
axis image
axis off

end