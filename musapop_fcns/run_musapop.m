function run_musapop(filename,bands_per_pixel)


disp('------------  MUSAPOP ALGORITHM ---------------')

%% load stuff
fig = 10;
plot_debug = false; % change this if you want to see intermediate states of the algorithm
scales = 3;

%% prepare data
disp('Loading data...')
load(['data\' filename],'Y','bin_width','T','h','scale_ratio','wavelengths')

if isempty(bin_width)
    bin_width = 1;
end

[~,attack] = max(h(:,1));
decay = size(h,1)-attack;

[Y,Y_ind,Nrow,Ncol,L] = convert_data_format(Y);

disp('Generating blue noise coded apertures...')
G = sampling_matrix(Nrow, Ncol, L, bands_per_pixel, 5);


disp('Subsampling data...')
[Y,Y_ind] = subsample(G,Y,Y_ind);

% save temporal data
save(['data\' filename 'scale_' num2str(1)],'Y','Y_ind','G')
Nrows = zeros(scales,1);
Ncols = zeros(scales,1);
scale_ratios = zeros(scales,1);
scale_ratios(1) = scale_ratio;
Nrows(1) = Nrow;
Ncols(1) = length(Y{1})/Nrow;

%% downsample 3 times
for j=1:scales-1
    [Y,Y_ind,T,G,Nrow,scale_ratio] = downsample_poisson(Y,Y_ind,T,G,3,Nrow,scale_ratio);
    % save temporal data
    save(['data\' filename 'scale_' num2str(1+j)],'Y','Y_ind','G');
    scale_ratios(1+j) = scale_ratio;
    Nrows(1+j) = Nrow;
    Ncols(1+j) = length(Y{1})/Nrow;
end

tic
%% Process
disp('Running algorithm...')
for i=scales:-1:1
    
    %% load data
    load(['data\' filename 'scale_' num2str(i)]);
    
    %% hyper-parameters
    [Nmc,lambda_area_int,log_gamma_area_int,alpha,Nbin] = get_hyperparam(length(Y{1}),i,scale_ratios(i));
    
    %% 3D search zone
    if i==scales
        [T0_prior,T0,A,B] = preprocess3D(Y,Y_ind,T,G,h,attack,decay,0,Nrows(i),Nbin);
        SBR = ones(1,L);   B_prior = ones([size(G),2]);
        B_prior(:,:,1) = 1;  B_prior(:,:,2) = 1e10;
    end
    
    %% build background prior
    if i==2
        [B_prior,SBR] = build_prior_B(Y,Y_ind,T,G,T0,Nrows(i),h,attack,decay);
    end
    
    %% process
    output = PointProcess_RJMCMC(Y,Y_ind,T,h,attack,decay,Nmc,T0_prior,T0,A,B,log_gamma_area_int,lambda_area_int,G,alpha,Nrows(i),scale_ratios(i),Nbin,plot_debug,B_prior,SBR,wavelengths);
    A = output{1}; T0 = output{2}; B = output{3};
    
    %% upsample estimates
    if i>1
        [T0,A,B,T0_prior,B_prior] = upsample_pointcloud(T0,A,B,T0_prior,B_prior,3,Nrows(i),scale_ratio,Nbin,Nrows(i-1),Ncols(i-1));
    end
    
end
elapsed_time = toc;
scale_ratio = scale_ratios(1);
Nrow = Nrows(1);

disp(['elapsed time: ' num2str(elapsed_time/60) 'minutes'])

disp('Saving reconstruction...')
%% save reconstruction
pcloud.depth = reshape(output{2},Nrow,length(output{2})/Nrow);
pcloud.spectra = reshape(output{1},Nrow,length(output{1})/Nrow);
pcloud.scale_ratio = scale_ratio;
pcloud.wavelengths = wavelengths;
pcloud.bin_width = bin_width;
B = reshape(output{3},Nrow,length(output{2})/Nrow,L);
save(['results\output_musapop_' filename],'pcloud','B','elapsed_time')
    
disp('Plotting reconstruction...')
%% plot 3D reconstruction
figure(fig)
plot_pcloud(pcloud);
figure(fig+1)
plot_bkg(B,pcloud.wavelengths);

%% plot error if ground_truth is available
gth_filename = ['data\ground_truth_' filename '.mat'];
if isfile(gth_filename)
   disp('Computing reconstruction quality...')
   figure(fig+2)
   max_distance = 10; % in mm
   compute_error(gth_filename,output,bin_width,max_distance);
end

end


