function [gdmd] = Run_claudiaCodesIter(N,shots)
n1 = 8;
n2 = n1;
w=[0.4 0.4 0.1 0.1];
% load RecSim_BooleanCodesLego256_shots=4_Rep=1_tau=8e-05; % For generating Figure for paper
% gdmd=gdmd(1:N,1:N,:);
% figure, 
% for i=1:shots
%     subplot(2,2,i),imagesc(gdmd(1:8,1:8,i));
% end
gdmd=DMD_Boolean(N,N,shots); %To start from a Boolean
iter = length(n1);
for ii=1:iter % Use several window sizes given by n1,n2
    gdmd = changelocalvh_claudia(gdmd,n1(ii),n2(ii),w);
end

% for i=1:shots
%     imagesc(gdmd(:,:,i)),pause
% end
