function fig=plot_triang(T0,A,scaleZ,dot_size,fig,Nrow,bin_width,wavelengths)


    if isempty(wavelengths)
        R = 1;
    else
        R = squeeze(spectrumRGB(wavelengths));
    end
    points=sum(cellfun(@(x) size(x,1),A));
    T0_plot = zeros(points,2);
    A_plot = zeros(size(T0_plot,1),3);
    Ncol=length(A)/Nrow;
    intensity=zeros(Nrow,Ncol);
    depth=zeros(Nrow,Ncol);
    k=1;
    min_t0 = Inf;
    
    max_A = 1; %max(max(exp(cell2mat(A))));
    
    for i=1:length(A)
        l=length(T0{i});
        if l>0
            T0_plot(k:k+l-1,1) = i;
            T0_plot(k:k+l-1,2) = T0{i};
            if min(T0{i})<min_t0
                min_t0 = min(T0{i});
            end
            
            A_plot(k:k+l-1,:) = exp(A{i})*R/max_A;   k=k+l;
            n_col=ceil(i/Nrow);
            n_row=i-Nrow*(n_col-1);
            if ~isempty(A{i})
                intensity(n_row,n_col) = max(mean(exp(A{i}),2));
                depth(n_row,n_col) = min(T0{i});
            end
        end
    end
    T0_plot(:,2) = T0_plot(:,2)-min_t0;
    
    
    x = (T0_plot(:,2))*bin_width;
    y = (Nrow-ceil(T0_plot(:,1)/Nrow))*scaleZ*bin_width;
    z = (Nrow - (T0_plot(:,1) - Nrow*(ceil(T0_plot(:,1)/Nrow)-1)))*scaleZ*bin_width;
    
%% COLORMAP
    %cmap = histeq(A_plot,cmap);
    cmap = jet(100);
    
    
%%   3D plot
    figure(fig)
    pcshow([x(:),y(:),z(:)],A_plot/mean(A_plot(:))/4,'MarkerSize',dot_size);
    set(gcf,'color','w');
    axis image
    axis on
    xlabel('[mm]')
%     colormap(cmap)
%     colorbar
   % caxis([0, .35]);
   
%% Intensity
    fig=fig+1;
    figure(fig);
    subplot(121)
    imAlpha=ones(size(intensity));
    imAlpha(intensity==0)=0;
    intensity(intensity==0)=min(intensity(intensity>0));
    imagesc(intensity,'AlphaData',imAlpha);
    set(gca,'color',[1 1 1]); 
    title('intensity')
    axis image
    colormap(cmap)
    colorbar
    axis off
    
%% Depth
    subplot(122)
    imAlpha=ones(size(depth));
    imAlpha(depth==0)=0;
    depth(depth==0)=min(depth(depth>0));
    imagesc(depth,'AlphaData',imAlpha);
    set(gca,'color',[1 1 1]); 
    colormap('jet')
    title('depth')
    axis image
    axis off
    colorbar
    

end