function [b,c] = compute_exp_approx(delta,u)

%a =1;
b = 1;
c = 1;


for i=1:3
    %a = a - (exp(u - delta).*(45.*delta.*u^2 + 6.*delta^2.*u + 6.*delta^3.*u - 15.*delta^2 - 15.*delta^3 - 3.*delta^4 + 45.*u^2 + 15.*delta^2.*u^2 + 4.*a.*delta^5.*exp(delta - u) + 15.*delta^2.*exp(delta + u).*exp(delta - u) - 15.*delta^3.*exp(delta + u).*exp(delta - u) + 3.*delta^4.*exp(delta + u).*exp(delta - u) - 45.*u^2.*exp(delta + u).*exp(delta - u) + 45.*delta.*u^2.*exp(delta + u).*exp(delta - u) - 6.*delta^2.*u.*exp(delta + u).*exp(delta - u) + 6.*delta^3.*u.*exp(delta + u).*exp(delta - u) - 15.*delta^2.*u^2.*exp(delta + u).*exp(delta - u)))/(4.*delta^5)
    b = b + (exp(u - delta).*(45.*u + 45.*delta.*u + 15.*delta^2.*u + 3.*delta^2 + 3.*delta^3 - 45.*u.*exp(delta + u).*exp(delta - u) - 2.*b.*delta^5.*exp(delta - u) - 3.*delta^2.*exp(delta + u).*exp(delta - u) + 3.*delta^3.*exp(delta + u).*exp(delta - u) - 15.*delta^2.*u.*exp(delta + u).*exp(delta - u) + 45.*delta.*u.*exp(delta + u).*exp(delta - u)))/(2.*delta^5);
    c = c - (exp(u - delta).*(45.*delta + 15.*delta^2 - 45.*exp(delta + u).*exp(delta - u) + 45.*delta.*exp(delta + u).*exp(delta - u) + 2.*c.*delta^5.*exp(delta - u) - 15.*delta^2.*exp(delta + u).*exp(delta - u) + 45))/(2.*delta^5);
end

end