function delta = delta_posterior(B,B_new,Y,T)


    delta = Y'*(B_new-B) - sum(T.*(exp(B_new)-exp(B)));

end