function [pixel,index] = choose_random_point(PPP,sum_PPP)

    s_PPP=sum(sum_PPP.*(1:length(sum_PPP))');
    u=rand*s_PPP;
    list=0; csum=0;
    while(u>csum)
        list=list+1;
        csum=csum+sum_PPP(list)*list;
    end
    
    list_pixel=randi(sum_PPP(list));
    p=PPP{list};
    pixel=p(list_pixel);
    index=randi(list);
end