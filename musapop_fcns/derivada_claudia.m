function [suma]=derivada_claudia(gdmd,N,n1,n2,ori,j,h)
% ori is the orientation of the diagonal: 1 for \ and 0 for /
% n1=n1-1;
% n2=n2-1;

p1=max(1,j-floor(n1/2));
p2=min(N,j+floor(n1/2));
p3=max(1,h-floor(n2/2));
p4=min(N,h+floor(n2/2));

u=gdmd(p1:p2,p3:p4);
if ori == 0
    u2=diag(flip(u,2));
else
    u2=diag(u);
end
suma=sum(u2(:));
