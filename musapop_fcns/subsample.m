function [Y,Y_ind] = subsample(G,Y,Y_ind)

[N,L] = size(G);

for n=1:N
    for l=1:L
        if G(n,l)==0
           Y{l}{n}=[];
           Y_ind{l}{n}=[];
        end
    end
end


end