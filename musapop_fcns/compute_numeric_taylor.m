function [u,c] = compute_numeric_taylor(B,Y,delta)

 %   f = @(B)  Y.*log(K+exp(B))-exp(B);

%     f = @(B)  -exp(B);
%     
%     fpd = f(B+delta);
%     fc = f(B);
%     fmd = f(B-delta);
    
    
    [u,c] = compute_exp_approx(delta,B);
    
    
    u = -u + Y;
    
%      c2 = - (fpd-2*fc+fmd)/(delta^2);
%      u2 = Y + (fpd-fmd)/(2*delta)+ c.*B;

end