function [NEIGH,neigh_sum]=find_all_neighbors(T0,occupied_volume,Nrow)

neigh_sum=zeros(8,1);
N=length(T0);

NEIGH=cell(N,1);

for n=1:N
   t0=T0{n};
   neigh=zeros(size(t0));
   n_col = ceil(n/Nrow);
   n_row = n-(n_col-1)*Nrow;
   
   for i=1:length(t0)
      neigh(i) = occupied_volume(n_row,n_col,t0(i))-1;
      if neigh(i)
          neigh_sum(neigh(i))=neigh_sum(neigh(i))+1;
      end
   end
   NEIGH{n}=neigh;
end

end