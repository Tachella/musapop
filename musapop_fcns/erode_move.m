function [A,T0,B,map_delta,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum, ...
    eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points] = ...
    erode_move(Y,Y_ind,T,h,attack,decay,A,T0,B,NEIGH,lambda_S,gamma_strauss,max_dist,occupied_volume, ...
    prior_length,PPP,total_points,integrated_h,Npix,Nbin,points_with_neigh,neigh_sum,prior_pixels,eff_prior_length,prob_dilate,prob_erode, ...
    sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels,B_prior,SBR)
    
    mdB = 0;
    map_delta = 0;
    L = size(B,2);
    
    %% pick a pixel and bin from ones with neighbors
    accepted=false;
    while ~accepted
        [pixel,index] = choose_random_point(PPP,sum_PPP);
        neigh=NEIGH{pixel};
        if neigh(index)
            accepted=true;
        end
    end
    
        
    %% kill the peak
    t0 = T0{pixel};
    a = A{pixel};
    b = B(pixel,:);
    
    a_prop=a;
    t0_prop=t0;

    a_prop(index,:)=[];
    t0_prop(index)=[];
    
    b_new=exp(a(index,:)).*integrated_h./SBR/T+b;
    u=b./b_new;

%     b_new = exp(a(index,:)).*integrated_h/T+b;
    
    %% proposal logprobability
    prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);
    
    prop = prop+sum(G(pixel,:).*exp(a(index,:)).*integrated_h.*(1-1./SBR));
    
    %% current logprobability
    curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);

    [prec, mean_a, det_term] = get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,L,false);
    curr = curr +sum(-1/2*prec.*(mean_a-a(index,:)).^2) + det_term;
    
    %% other GMRF term
    map_GMRF = pixelwise_B_prior(b_new,b,B_prior(pixel,:,1),B_prior(pixel,:,2)); 
    
    %% non sym dilation term
    n_col = ceil(pixel/Nrow);
    n_row = pixel-(n_col-1)*Nrow;
    total_neigh = occupied_volume(n_row,n_col,t0(index));
    term=-log((total_points+1)/(total_neigh-1))-log(2*Nbin+1);
    
    %% Poisson ref measure
    ref_measure = -log(lambda_S) + log(prior_length);
    
    %% non symmetrical proposal term
    sym =  term ...  % dilation proposal term 
     + log(points_with_neigh) ...% erosion proposal term 
     + sum(log(1-u)); 

    %% strauss term
    strauss=0;
    for j=1:length(t0_prop)
        if abs(t0(index)-t0_prop(j))<=max_dist
            strauss=strauss-log(gamma_strauss);
        end
    end
    
    %% area interaction
    log_penalty = area_interaction(1,occupied_volume,pixel,[],[],t0(index),Npix,Nbin,log_gamma_area_int,lambda_area_int);

    %% accept reject
    if rand < exp(ref_measure+prop-curr+strauss+log_penalty+map_GMRF+sym)
        A{pixel}=a_prop;
        T0{pixel}=t0_prop(:);
        B(pixel,:)=b_new;
        
        Mergeable_pixels = update_mergeable_list(t0_prop,t0,max(attack+decay),pixel,Mergeable_pixels);
        
        %remove pixel from list of size(a,1) points
        p=PPP{size(a,1)};
        p(p==pixel)=[];
        PPP{size(a,1)}=p;
        sum_PPP(size(a,1))=sum_PPP(size(a,1))-1;

        if ~isempty(a_prop) %if still has a point increment list
           PPP{size(a_prop,1)}=[PPP{size(a_prop,1)};pixel];
           sum_PPP(size(a_prop,1))=sum_PPP(size(a_prop,1))+1;
        end

        %% map delta
        map_delta=prop - curr + strauss + log_penalty + ref_measure+map_GMRF;
        mdB=map_GMRF;
        
        %% modify volume
        [occupied_volume,NEIGH,points_with_neigh,neigh_sum]=modify_volume(1,occupied_volume,pixel,[],t0(index),Npix,Nbin,NEIGH,T0,index,points_with_neigh,neigh_sum);
        eff_prior_length=modify_prior(1,prior_pixels,pixel,t0(index),max_dist,eff_prior_length);
    
        total_points=total_points-1;
        K(pixel,size(a_prop,1)+1)=K(pixel,size(a_prop,1)+1)+1;
    else
        K(pixel,size(a,1)+1)=K(pixel,size(a,1)+1)+1;
    end
    
    
end