function gdmd=changelocalvh_claudia(gdmd,n1,n2,w)

[N1,N2,S]=size(gdmd);
N=N1;
J=randperm(N);
H=randperm(N);
% J=1:N1;
% H=1:N2;

for j=1:length(J)
    for h=1:length(H)
        
        for r=1:S
            a2=zeros(1,S);
            a2(r)=1;
            gdmd(J(j),H(h),:)=a2(:);
            
            [suma] = neigvorlocal_claudia(gdmd(:,:,r),N,1,n2,J(j),H(h));
            [suma2] = neigvorlocal_claudia(gdmd(:,:,r),N,n1,1,J(j),H(h));
            [suma3(r)] = neigvorlocal_claudia(gdmd(:,:,r),N,n1,n2,J(j),H(h));
            [suma5]=derivada_claudia(gdmd(:,:,r),N,n1,n2,0,J(j),H(h));
            [suma6]=derivada_claudia(gdmd(:,:,r),N,n1,n2,1,J(j),H(h));            
            %             ya=4*((std(suma)+std(suma2)));
            %             ss(r)=ya;
            %             ye=4*(sum(suma6(:))+n1/n2*sum(suma5(:)));
            %             ss(r)=ss(r)+ye;
            ss(r)=w(1)*suma+w(2)*suma2+w(3)*suma5+w(4)*suma6;
            %ss(r)=0.5*suma+0.3*suma2+0.1*suma5+0.1*suma6;
        end
        
        [a]=find(ss==min(ss));
        if length(a)>1
            suma3=suma3(a);
            [b]=find(suma3==min(suma3));
            if length(b)==1
                gdmd(J(j),H(h),:)=0;
                gdmd(J(j),H(h),a(b))=1;
            else
                u=randperm(length(a));
                gdmd(J(j),H(h),:)=0;
                gdmd(J(j),H(h),a(u(1)))=1;
            end
        else
            gdmd(J(j),H(h),:)=0;
            gdmd(J(j),H(h),a)=1;
        end
        %u=randperm(length(a));
        %gdmd(J(j),H(h),:)=0;
        %gdmd(J(j),H(h),a(u(1)))=1;
    end
end

