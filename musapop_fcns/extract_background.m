function [YB,YS,Tb] = extract_background(T0,Y,Y_ind,T,attack,decay)

YB = zeros(length(Y),1);
YS = zeros(length(Y),1);
Tb = zeros(length(Y),1);

for n=1:length(Y)
    
    b_region = ones(T,1);
    t0 = T0{n};
    for j=1:length(t0)
        ind = max([1,t0(j)-2*attack]):min([T,t0(j)+decay-1]);
        b_region(ind) = 0;
    end
    Tb(n) = sum(b_region);
    
    s_region = -1*(b_region-1);
    if ~isempty(Y{n})
        YB(n) = Y{n}*b_region(Y_ind{n});
        YS(n) = Y{n}*s_region(Y_ind{n});
        %ind = logical(b_region(Y_ind{n}));
        %Y{n} = Y{n}(ind);
        %Y_ind{n} = Y_ind{n}(ind);
    end
end

end