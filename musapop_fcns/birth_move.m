function [A,T0,B,map_delta,mdB,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points]=birth_move(Y,Y_ind,T,h,attack,decay,A,T0,B,prior_pixels,lambda_S,gamma_strauss,max_dist,occupied_volume,prior_length,prior_PPP,prior_order,PPP,total_points,integrated_h,Npix,Nbin,beta_par,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels,B_prior,SBR)
  
    mdB=0;
    map_delta=0;
    L = size(B,2);
    
    %% pick a bin
    strauss=Inf;
    while isinf(strauss)
        pixel = prior_PPP(randi(length(prior_PPP)));
        p=prior_pixels{pixel};
        t0_new=p(randi(length(p)));    

        %% strauss term
        t0=T0{pixel};
        strauss=0;
        for j=1:length(t0)
            if abs(t0_new-t0(j)) <= max_dist
                strauss = strauss + log(gamma_strauss);
                break %only if hardconstraint
            end
        end
    end
    
    if ~isinf(strauss)

        a = A{pixel};
        b = B(pixel,:);
        
        %% propose a peak
        u = rand(1,L);
        a_new = log(SBR.*b.*(1-u)*T./integrated_h);

        b_new = u.*b;

        a_prop = a;
        t0_prop = t0;

        a_prop(end+1,:) = a_new;
        t0_prop(end+1,:) = t0_new;

        %% proposal logprobability
        prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);
        
        [prec, mean_a, det_term] = get_GP_par(t0_new,pixel,A,T0,alpha,scale_Z,Nrow,Nbin,L,false);
    
        prop = prop + sum(- 1/2*(a_new-mean_a).^2.*prec) + det_term;
        
        prop = prop - sum(G(pixel,:).*exp(a_new).*integrated_h.*(1-1./SBR));

        %% current log-probability
        curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);

        %% other GMRF term
        map_GMRF = pixelwise_B_prior(b_new,b,B_prior(pixel,:,1),B_prior(pixel,:,2)); 

        %% non symmetrical proposal term
        % jacobian 1/(1-u)
        sym = log(lambda_S) - log(total_points+1) - sum(log(1-u)) - log(eff_prior_length) + log(prior_length); 

        %% area interaction
        log_penalty = area_interaction(0,occupied_volume,pixel,t0_new,[],[],Npix,Nbin,log_gamma_area_int,lambda_area_int);

        %% accept/reject
        if rand<exp(prop  - curr  + strauss + log_penalty + map_GMRF + sym)
            
            %% save new estimates
            A{pixel} = a_prop;
            T0{pixel} = t0_prop;
            B(pixel,:) = b_new;
            
            Mergeable_pixels = update_mergeable_list(t0_prop,t0,max(attack)+max(decay),pixel,Mergeable_pixels);
            
            if ~isempty(a) %remove pixel from list of size(a,1) points
               p=PPP{size(a,1)};
               p(p==pixel)=[];
               PPP{size(a,1)}=p;
               sum_PPP(size(a,1))=sum_PPP(size(a,1))-1;
            end
            
            PPP{size(a_prop,1)}=[PPP{size(a_prop,1)};pixel];
            sum_PPP(size(a_prop,1))=sum_PPP(size(a_prop,1))+1;
            
            
            %% compute map
            map_delta = prop-curr+strauss+log_penalty+log(lambda_S/prior_length)+map_GMRF;
            
            mdB = map_GMRF;
            
            %% modify volume
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum] = modify_volume(0,occupied_volume,pixel,t0_new,[],Npix,Nbin,NEIGH,T0,[],points_with_neigh,neigh_sum);
            eff_prior_length = modify_prior(0,prior_pixels,pixel,t0_new,max_dist,eff_prior_length);
           
            total_points = total_points+1;
            K(pixel,size(a_prop,1)+1) = K(pixel,size(a_prop,1)+1)+1;
        else
            K(pixel,size(a,1)+1)=K(pixel,size(a,1)+1)+1;
        end
    end
    
end