function curr = compute_likelihood_mark(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay)

L = size(G,2);
g = G(pixel,:);
curr = zeros(1,L);
for l=1:L
    if g(l)>0
        ind = Y_ind{l}{pixel};
        if ~isempty(isempty(ind))
            x = g(l)*b(l)*ones(length(ind),1);
            for j=1:length(t0)
                i1=find(ind>t0(j)-attack & ind<t0(j)+decay);
                x(i1)=x(i1)+g(l)*exp(a(j,l))*h(ind(i1)-t0(j)+attack,l);
            end
            curr(l) = curr(l) + Y{l}{pixel}*log(x);
        end
    end
end

end