function delta = delta_gmrf(B_new,B,c,u)

delta =  ((c.*B_new)'*B_new)/2 - ((c.*B)'*B)/2 - (B_new-B)'*u;

end