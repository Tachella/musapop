function [A,T0,map_delta,occupied_volume,PPP,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,K,Mergeable_pixels,total_points] = split_move(Y,Y_ind,T,h,attack,decay,A,T0,B,T0_prior,lambda_S,gamma_strauss,max_dist,occupied_volume,prior_length,prior_PPP,prior_order,PPP,total_points,Npix,Nbin,NEIGH,points_with_neigh,neigh_sum,eff_prior_length,sum_PPP,log_gamma_area_int,lambda_area_int,G,alpha,scale_Z,K,Nrow,Mergeable_pixels,beta_ms)
  

    map_delta = 0;
    
    [pixel,index] = choose_random_point(PPP,sum_PPP);
    
    L = size(G,2);
    t0 = T0{pixel}; 
    delta = randi([max_dist+1,max(attack+decay)]);
    u = betarnd(beta_ms,beta_ms,1,L);
    a = A{pixel};
    
    t0_new_1 = round( t0(index)-delta * sum(exp(a(index,:)).*(1-u))/sum(exp(a(index,:))) );
    t0_new_2 = round( t0(index)+delta * sum(exp(a(index,:)).*u)/sum(exp(a(index,:))) );
    
    t0_prop = t0;
    t0_prop(index)=[];
    
    strauss=0;
    for j=1:length(t0_prop)
        if abs(t0_new_2-t0_prop(j))<=max_dist || abs(t0_new_1-t0_prop(j))<=max_dist
            strauss=strauss+log(gamma_strauss);
            break %only if hardconstraint
        end
    end
        
    if  ~isinf(strauss) && sum((t0_new_1-T0_prior{pixel})==0) && sum((t0_new_2-T0_prior{pixel})==0) %if the proposal lies inside the prior
        
        b = B(pixel,:);
        
        %% propose a peak
        a_new_1 = a(index,:) + log(u);
        a_new_2 = a(index,:) + log(1-u);
        
        b_new = b;

        a_prop = a;

        a_prop(index,:)=[];
        
        if isempty(t0_prop)
            t0_prop(1,1)=t0_new_1;
            a_prop(1,:)=a_new_1;
        else
            t0_prop(end+1,1)=t0_new_1;
            a_prop(end+1,:)=a_new_1;
        end
        t0_prop(end+1,1)=t0_new_2;
        a_prop(end+1,:)=a_new_2;

        %% proposal logprobability
        prop = compute_likelihood(Y,Y_ind,G,pixel,b_new,t0_prop,a_prop,h,attack,decay);
        
        [prec, mean_a, det_term] = get_GP_par(t0_new_1,pixel,A,T0,alpha,scale_Z,Nrow,Nbin,L,false);
        [prec_2, mean_a_2, det_term2] = get_GP_par(t0_new_2,pixel,A,T0,alpha,scale_Z,Nrow,Nbin,L,false);
    
        prop = prop +sum(-(a_new_1-mean_a).^2.*prec/2-(a_new_2-mean_a_2).^2.*prec_2/2)+det_term+det_term2;

        %% current logprobability
        curr = compute_likelihood(Y,Y_ind,G,pixel,b,t0,a,h,attack,decay);

        [prec, mean_a, det_term] = get_GP_par(t0(index),pixel,A,T0,alpha,scale_Z,Nrow,Nbin,L,false);
        curr= curr +sum(-(a(index,:)-mean_a).^2.*prec/2)+ det_term;

        %% poisson ref meas
        ref_meas = log(lambda_S)-log(prior_length);
        
        %% non symmetrical proposal term
        %jacobian 1/(u-u^2) (INSIDE BETA PDF FOR SIMPLICITY)
        sym = log(sum(cellfun(@length,Mergeable_pixels)))-log(total_points+1)+log(max_dist+attack+decay)-sum((beta_ms-1+1)*(log(u)+log(1-u)))+L*2*gammaln(beta_ms)-L*gammaln(2*beta_ms);
            
        %% area interaction
        log_penalty = area_interaction(3,occupied_volume,pixel,t0_new_1,t0_new_2,t0(index),Npix,Nbin,log_gamma_area_int,lambda_area_int);
        
        %% accept/reject
        if rand < exp(prop+ref_meas-curr+strauss+log_penalty+sym)
            %% save new estimates
            A{pixel}=a_prop;
            T0{pixel}=t0_prop;
            
            Mergeable_pixels = update_mergeable_list(t0_prop,t0,max(attack+decay),pixel,Mergeable_pixels);

            if ~isempty(a) %remove pixel from list of size(a,1) points
               p=PPP{size(a,1)};
               p(p==pixel)=[];
               PPP{size(a,1)}=p;
               sum_PPP(size(a,1))=sum_PPP(size(a,1))-1;
            end
            PPP{size(a_prop,1)}=[PPP{size(a_prop,1)};pixel];
            sum_PPP(size(a_prop,1))=sum_PPP(size(a_prop,1))+1;
            
            %% compute map
            map_delta=prop-curr+strauss+log_penalty+ref_meas;
            
            %% modify volume
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum] = modify_volume(1,occupied_volume,pixel,[],t0(index),Npix,Nbin,NEIGH,T0,index,points_with_neigh,neigh_sum);
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum] = modify_volume(0,occupied_volume,pixel,t0_new_1,[],Npix,Nbin,NEIGH,T0,[],points_with_neigh,neigh_sum);
            [occupied_volume,NEIGH,points_with_neigh,neigh_sum] = modify_volume(0,occupied_volume,pixel,t0_new_2,[],Npix,Nbin,NEIGH,T0,[],points_with_neigh,neigh_sum);
            
            total_points = total_points+1;
            eff_prior_length = modify_prior(0,T0_prior,pixel,t0_new_2,max_dist,eff_prior_length);
           
            K(pixel,size(a_prop,1)+1) = K(pixel,size(a_prop,1)+1)+1;
        else
            K(pixel,size(a,1)+1) = K(pixel,size(a,1)+1)+1;
        end
    end
    
end