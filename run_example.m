clear all; close all; clc;
%% READ ME FIRST
% Paper: 
% Author List: J. Tachella,	Y. Altmann,	M. Marquez, H. Arguello-Fuentes
% J.-Y. Tourneret, 	and S. McLaughlin
% Contact: J. Tachella, email: jat3@hw.ac.uk
%% DATA INPUT
% A Lidar dataset (of size Nr x Nc x T) is saved in a .mat file in the data folder and must
% have the following variables:
% Y (type: cell) size(Y)= [Nr,Nc,T] : Lidar cube of Nr by Nc pixels and T
% histogram bins
% h (type: double) size(h) = [length of impulse response, 1] : vector
% containing impulse response
% G (type: double) size(G) = [Nr,Nc] : pixel wise gain of the Lidar 
% device, between [0,1]. It can also be used to model any compressive
% sensing strategy. By default set to G = ones(Nr,Nc);
% bin_width (type: double) size(bin_width)=1 : bin width in millimetres, it
% can be obtained as speed_of_light/2*TCSPC_binning
% scale_ratio (type: double) size(scale_ratio)=1 : approximate ratio
% between the width of a pixel and the width of a bin, computed as
% pixel_width/bin_width

%% OUTPUT
% The reconstructed 3D point cloud is saved in the folder \results with
% .ply formatting.
% Additionally, a .mat file is saved with the following variables:
% p (type: pointCloud) : Point Cloud with points in real-world coordinates
% (same as .ply file)
% intensity_im (type: double) size(intensity_im) = Nr x Nc x L, spectral intensity image
% keeps only one point per pixel (the one with highest intensity)
% depth_im (type: double) size(intensity_im) = Nr x Nc, depth image
% keeps only one point per pixel (the one with highest intensity)
% elapsed_time (type: double) size(elapsed_time) = 1, contains execution
% time in seconds
% B: background image of size Nr x Nc x L


%% Examples: just uncomment the desired dataset and run this script (don't include .mat)
filename = 'middlebury_art';

%% Chose number of measured bands per pixel: (in the paper denoted by W)
% This value has to be a power of 2 and smaller or equal than the total 
% number of wavelengths
bands_per_pixel = 2;

addpath('musapop_fcns')
run_musapop(filename, bands_per_pixel)
